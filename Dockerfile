FROM rust:latest

# install jupyter and rust kernel
RUN useradd -m rust
RUN apt update && apt install -y \
    jupyter-notebook \
    cmake
RUN cargo install evcxr_jupyter

# Setup user
RUN chown -R 1000:1000 /usr/local/cargo
USER 1000:1000

# configure rust kernel
RUN evcxr_jupyter --install

# Run jupyter notebook
CMD jupyter notebook --ip "0.0.0.0"
